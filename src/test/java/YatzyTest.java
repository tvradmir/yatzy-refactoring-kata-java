import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.*;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class YatzyTest {

    @Test
    @Parameters({
        "2,3,4,5,1,15",
        "3,3,4,5,1,16"})
    public void should_return_sum_of_all_dice_when_chance(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).chance());
    }

    @Test
    @Parameters({
        "4,4,4,4,4,50",
        "6,6,6,6,6,50",
        "6,6,6,6,3,0"})
    public void should_return_50_when_yatzy(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).yatzy());
    }

    @Test
    @Parameters({
        "1,2,3,4,5,1",
        "1,2,1,4,5,2",
        "6,2,2,4,5,0",
        "1,2,1,1,1,4"})
    public void should_return_sum_of_all_ones_when_ones(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).ones());
    }

    @Test
    @Parameters({
        "1,2,3,2,6,4",
        "2,2,2,2,2,10",
        "6,3,3,4,5,0"})
    public void should_return_sum_of_all_twos_when_twos(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).twos());
    }

    @Test
    @Parameters({
        "1,2,3,2,3,6",
        "2,3,3,3,3,12",
        "6,2,2,4,5,0"})
    public void should_return_sum_of_all_threes_when_threes(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).threes());
    }

    @Test
    @Parameters({
        "4,4,4,5,5,12",
        "4,4,5,5,5,8",
        "4,5,5,5,5,4",
        "2,5,5,5,5,0"})
    public void should_return_sum_of_all_fours_when_fours(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).fours());
    }

    @Test
    @Parameters({
        "4,4,4,5,5,10",
        "4,4,5,5,5,15",
        "4,5,5,5,5,20",
        "4,4,4,4,4,0"})
    public void should_return_sum_of_all_fives_when_fives(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).fives());
    }

    @Test
    @Parameters({
        "4,4,4,5,5,0",
        "4,4,6,5,5,6",
        "6,5,6,6,5,18"})
    public void should_return_sum_of_all_sixes_when_sixes(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).sixes());
    }

    @Test
    @Parameters({
        "3,4,3,5,6,6",
        "5,3,3,3,5,10",
        "5,3,6,6,5,12",
        "1,2,3,4,5,0"})
    public void should_return_sum_of_two_highest_matching_values_when_pair(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).pair());
    }

    @Test
    @Parameters({
        "3,3,5,4,5,16",
        "3,3,5,5,5,16",
        "1,1,2,3,4,0"})
    public void should_return_sum_of_two_doubled_values_when_two_pair(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).twoPair());
    }

    @Test
    @Parameters({
        "3,3,3,4,5,9",
        "5,3,5,4,5,15",
        "3,3,3,3,5,9",
        "3,3,4,5,6,0",
        "3,3,3,3,3,9"})
    public void should_return_sum_of_tripled_values_when_three_of_kind(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).threeOfAKind());

    }

    @Test
    @Parameters({
        "3,3,3,3,5,12",
        "5,5,5,4,5,20",
        "2,2,2,5,5,0"})
    public void should_return_sum_of_quadrupled_values_when_four_of_kind(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).fourOfAKind());
    }

    @Test
    @Parameters({
        "1,2,3,4,5,15",
        "2,3,4,5,1,15",
        "1,2,2,4,5,0"})
    public void should_return_sum_of_consecutive_number_from_1_to_5_when_small_straight(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).smallStraight());
    }

    @Test
    @Parameters({
        "6,2,3,4,5,20",
        "2,3,4,5,6,20",
        "1,2,2,4,5,0"})
    public void should_return_sum_of_consecutive_number_from_2_to_6_when_large_straight(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).largeStraight());
    }

    @Test
    @Parameters({
        "6,2,2,2,6,18",
        "2,3,4,5,6,0"})
    public void  should_return_sum_of_pair_and_three_of_kind_when_full_house(int d1, int d2, int d3, int d4, int d5, int expected) {
        assertEquals(expected, new Yatzy(d1, d2, d3, d4, d5).fullHouse());
    }
}
