import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Map.Entry.*;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

public class Yatzy {

    protected int[] dice;
    public Yatzy(int d1, int d2, int d3, int d4, int d5) {
        dice = new int[5];
        dice[0] = d1;
        dice[1] = d2;
        dice[2] = d3;
        dice[3] = d4;
        dice[4] = d5;
    }

    public int chance()
    {
        return IntStream.of(dice).sum();
    }

    public  int yatzy()
    {
        return  IntStream.of(dice).distinct().count()==1 ? 50 : 0;
    }

    public  int ones() {
        return sumDiceValuesEqualTo(1);
    }

    public  int twos() {
        return sumDiceValuesEqualTo(2);
    }

    public  int threes() {
        return sumDiceValuesEqualTo(3);
    }

    public int fours()
    {
        return sumDiceValuesEqualTo(4);
    }

    public int fives() {
        return sumDiceValuesEqualTo(5);
    }

    public int sixes()
    {
        return sumDiceValuesEqualTo(6);
    }

    public  int pair() {
        Optional<Map.Entry<Integer, Long>> result = getDiceValuesByOccurrences( 1)
            .max(comparingByKey());

        return result.isPresent() ? result.get().getKey() * 2 : 0 ;

    }

    public  int twoPair() {
         List<Integer> pairValues = getDiceValuesByOccurrences(1)
            .map(e -> e.getKey() * 2).collect(toList());

         return pairValues.size() > 1 ? pairValues.stream().reduce(0, (res, e) -> res + e) : 0;

    }

    public  int threeOfAKind() {
        return sumOfDiceValueOccursMoreThan(3, 2);

    }

    public int fourOfAKind() {
        return sumOfDiceValueOccursMoreThan(4, 3);

    }


    public int smallStraight() {
        int chance = chance();
        return chance == 15 ? chance : 0;
    }

    public  int largeStraight() {
        int chance = chance();
        return chance == 20 ? chance : 0;
    }

    public  int fullHouse() {
        return  isFullHouse() ? IntStream.of(dice).sum() : 0;

    }

    /**
     * sum of values equal to given diceValue
     * @param diceValue
     * @return
     */
    private  int sumDiceValuesEqualTo(int diceValue) {
        return IntStream.of(dice).filter(i -> i == diceValue).sum();
    }


    /**
     * Method that returns Stream of Map dice value occurs more than given occurrence
     * @param occurrence
     * @return
     */
    private  Stream<Map.Entry<Integer, Long>> getDiceValuesByOccurrences(int occurrence) {
        return IntStream.of(dice).boxed()
            .collect(groupingBy(identity(), counting()))
            .entrySet().stream().filter(s -> s.getValue() > occurrence);
    }

    /**
     * Sum of values occurs more than given occurrence
     * @param diceValue
     * @param occurrences
     * @return
     */
    private Integer sumOfDiceValueOccursMoreThan(int diceValue, int occurrences) {
        return getDiceValuesByOccurrences(occurrences)
            .map(e -> e.getKey() * diceValue).reduce(0, (res, e) -> res + e);
    }

    private  boolean isFullHouse() {

        var twoAndThreeOfKind = new ArrayList<>(Arrays.asList(2L, 3L));

        boolean fullHouse = getDiceValuesByOccurrences(1)
            .filter(e->twoAndThreeOfKind.remove(e.getValue()))
            .anyMatch(__ -> twoAndThreeOfKind.isEmpty());

        return fullHouse;
    }
}



